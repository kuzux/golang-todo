package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"text/template"
)

// each template file is loaded into a string, then a "partials" thing is created like
// {define "a.html"} This is template A {end} {define "b.html"} This is template B {end}
// then, for example, template b.html is loaded like
// { partials } This is template A
// the result is then parsed and stored into templates["a"]
var templateSrcs = make(map[string]string)
var templates = make(map[string]*template.Template)

func loadTemplateSrcs(templatesPath string) error {
	files, err := os.ReadDir(templatesPath)
	if err != nil {
		return err
	}

	for _, fileInfo := range files {
		// TODO: Check if the file is an html file
		filePath := filepath.Join(templatesPath, fileInfo.Name())
		bytes, err := os.ReadFile(filePath)
		if err != nil {
			return fmt.Errorf("error reading template file %s: %v", fileInfo.Name(), err)
		}

		name := fileInfo.Name()
		nameWithoutExt := name[:len(name)-len(filepath.Ext(name))]

		templateSrcs[nameWithoutExt] = string(bytes)
	}

	return nil
}

func generateTemplatePartials() string {
	var partials string
	for name, src := range templateSrcs {
		partials += fmt.Sprintf("{{define \"%s\"}} \n%s \n{{end}}\n\n", name, src)
	}
	return partials
}

func loadTemplates(templatesPath string) error {
	err := loadTemplateSrcs(templatesPath)
	if err != nil {
		return err
	}
	partials := generateTemplatePartials()
	for name, src := range templateSrcs {
		src = fmt.Sprintf("%s\n%s", partials, src)
		templateName := fmt.Sprintf("%s.html", name)
		tmpl, err := template.New(templateName).Parse(src)
		if err != nil {
			log.Printf("Template source %s\n", src)
			return fmt.Errorf("error parsing template %s: %v", name, err)
		}
		nameWithoutExt := name[:len(name)-len(filepath.Ext(name))]
		templates[nameWithoutExt] = tmpl
	}

	return nil
}

func ReloadTemplates() {
	log.Println("Reloading templates...")
	err := loadTemplates("templates")
	if err != nil {
		log.Fatalf("%v\n", err)
	}
}
