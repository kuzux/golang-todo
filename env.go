package main

import (
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func GetNodeVersion() (int, error) {
	bytes, err := exec.Command("node", "-v").Output()
	if err != nil {
		return 0, err
	}

	versionString := strings.TrimSpace(string(bytes))
	majorVersion := strings.SplitN(versionString, ".", 2)[0]

	if majorVersion[0] == 'v' {
		majorVersion = majorVersion[1:]
	}

	res, err := strconv.Atoi(majorVersion)

	return res, err
}

func GetBooleanEnv(key string, defaultValue bool) bool {
	value := os.Getenv(key)

	value = strings.ToLower(value)
	if value == "true" || value == "1" || value == "yes" || value == "on" {
		return true
	}
	if value == "false" || value == "0" || value == "no" || value == "off" {
		return false
	}
	return defaultValue
}

func GetIntegerEnv(key string, defaultValue int) int {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}

	res, err := strconv.Atoi(value)
	if err != nil {
		return defaultValue
	}
	return res
}

func GetStringEnv(key string, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	return value
}
