package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/jwtauth/v5"
	"github.com/lestrrat-go/jwx/v2/jwt"
)

func HandleAddItem(w http.ResponseWriter, r *http.Request) {
	db := GetDbFromContext(r.Context())
	name := r.FormValue("name")
	if name == "" {
		w.WriteHeader(http.StatusNotModified)
		return
	}

	item, err := AddItem(db, name)
	if err != nil {
		log.Fatalf("error adding items: %v\n", err)
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	templates["item"].Execute(w, &item)
}

func HandleCompleteItem(w http.ResponseWriter, r *http.Request) {
	db := GetDbFromContext(r.Context())
	id := r.FormValue("id")
	if id == "" {
		w.WriteHeader(http.StatusNotModified)
		return
	}
	completed := r.FormValue("completed")

	item, err := CompleteItem(db, id, (completed == "on"))
	if err != nil {
		log.Fatalf("error completing item: %v\n", err)
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	templates["item"].Execute(w, &item)
}

func HandleSignup(w http.ResponseWriter, r *http.Request) {
	db := GetDbFromContext(r.Context())
	username := r.FormValue("username")
	password := r.FormValue("password")

	if username == "" || password == "" {
		w.WriteHeader(http.StatusNotModified)
		// TODO: show error message

		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		templates["signup"].Execute(w, nil)
		return
	}

	_, err := Signup(db, username, password)
	if err != nil {
		log.Printf("error signing up: %v\n", err)
	}

	token, err := Login(db, username, password)
	if err != nil {
		log.Fatalf("error logging in: %v\n", err)
	}

	cookie := http.Cookie{Name: "jwt", Value: token, HttpOnly: true}
	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/", http.StatusFound)
}

func HandleLogin(w http.ResponseWriter, r *http.Request) {
	db := GetDbFromContext(r.Context())
	username := r.FormValue("username")
	password := r.FormValue("password")

	log.Print("handling logging in")

	if username == "" || password == "" {
		// TODO: show error message
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		templates["login"].Execute(w, nil)
		return
	}

	token, err := Login(db, username, password)
	if err != nil {
		// TODO: show error message
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		templates["login"].Execute(w, nil)
		return
	}

	cookie := http.Cookie{Name: "jwt", Value: token, HttpOnly: true}
	http.SetCookie(w, &cookie)
}

func HandleHomePage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	token, _, err := jwtauth.FromContext(r.Context())
	if err != nil {
		log.Printf("error getting token in home page: %v\n", err)
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}

	if token == nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}

	err = jwt.Validate(token)
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}

	http.Redirect(w, r, "/items", http.StatusFound)
}

func main() {
	nodeVersion, err := GetNodeVersion()
	if err != nil {
		log.Fatal(err)
	}
	if nodeVersion <= 12 {
		log.Fatal("Node version must be greater than 12 for tailwind to work correctly")
	}

	port := GetIntegerEnv("PORT", 8080)
	watch := GetBooleanEnv("WATCH", true)
	dbPath := GetStringEnv("DB_PATH", "todo.db")

	ReloadTemplates()
	RebuildTailwind()

	if watch {
		go SetupWatcher()
	}

	err = CreateDatabase(dbPath)
	if err != nil {
		log.Fatal(err)
	}

	r := chi.NewRouter()
	// A good base middleware stack
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Compress(5))
	r.Use(DbMiddleware(dbPath))
	r.Use(middleware.Recoverer)

	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped.
	r.Use(middleware.Timeout(30 * time.Second))

	fs := http.FileServer(http.Dir("static"))
	r.Handle("/static/*", http.StripPrefix("/static/", fs))

	r.Group(func(r chi.Router) {
		r.Use(LoginVerifier())
		r.Use(jwtauth.Authenticator)

		r.Get("/items", func(w http.ResponseWriter, r *http.Request) {
			db := GetDbFromContext(r.Context())
			data, err := GetItems(db)
			if err != nil {
				log.Fatalf("error getting items: %v\n", err)
			}

			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			templates["index"].Execute(w, &data)
		})

		r.Post("/add-item", HandleAddItem)
		r.Post("/complete-item", HandleCompleteItem)

		r.Get("/logout", func(w http.ResponseWriter, r *http.Request) {
			// TODO: This should be a POST request
			cookie := http.Cookie{Name: "jwt", Value: "", HttpOnly: true}
			http.SetCookie(w, &cookie)
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		})
	})

	r.Get("/signup", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		templates["signup"].Execute(w, nil)
	})
	r.Post("/signup", HandleSignup)

	r.Get("/login", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		templates["login"].Execute(w, nil)
	})
	r.Post("/login", HandleLogin)

	r.Group(func(r chi.Router) {
		r.Use(LoginVerifier())
		// we do the authorization logic in the handler itself
		r.Get("/", HandleHomePage)
	})

	log.Printf("Listening on port %d...\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), r))
}
