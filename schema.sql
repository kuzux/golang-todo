create table if not exists todo_items (
    id text primary key,
    name text,
    completed boolean
);
create table if not exists users (
    id text primary key,
    name text,
    hashed_password text
);