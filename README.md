# todo app thingy

* yep, the classic one. 
* written with golang and htmx. 
* uses tailwind css. 
* uses sqlite for persistence
* intended to be a template for future applications

## usage

Just running `./m` starts a server at port 8080. The options are supplied via environment variables. 
* `PORT=3456 ./m`: start the server at a different port (defaults to 8080)
* `WATCH=no ./m`: start the server without a watcher for template files (defaults to yes)
* `DB_PATH=other.db ./m` start the server with a custom sqlite3 db path. Can be set to `:memory:` (defaults to `todo.db`)

## database

* on running for the first time, a sqlite3 database is created with the given `schema.sql` file
* as for db migrations, they should be handled manually
