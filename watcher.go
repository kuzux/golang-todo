package main

import (
	"log"
	"os/exec"

	"github.com/fsnotify/fsnotify"
)

func RebuildTailwind() {
	log.Println("Rebuilding Tailwind CSS...")

	exec.Command("npx", "tailwindcss",
		"-i", "main.css",
		"-o", "static/main.css",
		"-m").Wait()

	log.Println("Rebuilt Tailwind CSS.")
}

func SetupWatcher() {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	go func() {
		log.Println("Watching for changes...")
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Has(fsnotify.Write) {
					log.Println("modified file:", event.Name)
					// TODO: Only reload the changed templates
					ReloadTemplates()
					RebuildTailwind()
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()

	err = watcher.Add("templates")
	if err != nil {
		log.Fatal(err)
	}
	err = watcher.Add("main.css")
	if err != nil {
		log.Fatal(err)
	}

	// Block main goroutine forever.
	<-make(chan struct{})
}
