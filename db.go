package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/go-chi/jwtauth/v5"
	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

func CreateDatabase(path string) error {
	_, err := os.Stat(path)
	dbAlreadyExists := (err == nil || !os.IsNotExist(err))
	db, err := sql.Open("sqlite3", path)
	if err != nil {
		return fmt.Errorf("error reading db: %v", err)
	}
	if dbAlreadyExists {
		log.Printf("Database already exists at %s\n", path)
		// Should we check if the db schema matches the schema defined in the file?

		return nil
	}
	log.Printf("Creating new database at %s\n", path)

	sqlBytes, err := os.ReadFile("schema.sql")
	if err != nil {
		return err
	}
	sqlString := string(sqlBytes)

	// It should be fairly safe to assume no non-separator semicolons will be in the schema SQL
	// FIXME: But that is technically a possibility
	stmts := strings.Split(sqlString, ";")
	for _, stmt := range stmts {
		log.Printf("Executing sql statement: %s\n", stmt)
		_, err := db.Exec(stmt)
		if err != nil {
			return fmt.Errorf("error executing sql statement %s: %v", stmt, err)
		}
	}

	return nil
}

type CtxKey string

func DbMiddleware(path string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			db, err := sql.Open("sqlite3", path)
			if db == nil || err != nil {
				log.Fatalf("error reading db: %v\n", err)
			}
			defer db.Close()

			ctx := r.Context()
			ctx = context.WithValue(ctx, CtxKey("db"), db)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func GetDbFromContext(ctx context.Context) *sql.DB {
	return ctx.Value(CtxKey("db")).(*sql.DB)
}

type Item struct {
	Id        string
	Name      string
	Completed bool
}

func AddItem(db *sql.DB, name string) (Item, error) {
	log.Printf("Adding item: %s\n", name)
	id := uuid.New().String()
	_, err := db.Exec("INSERT INTO todo_items (id, name, completed) VALUES (?, ?, ?)", id, name, false)
	return Item{id, name, false}, err
}

func CompleteItem(db *sql.DB, id string, completed bool) (Item, error) {
	log.Printf("Completing item: %s %v\n", id, completed)
	_, err := db.Exec("UPDATE todo_items SET completed = ? WHERE id = ?", completed, id)
	if err != nil {
		return Item{}, err
	}
	row, err := db.Query("SELECT name FROM todo_items WHERE id = ?", id)
	if err != nil {
		return Item{}, err
	}
	defer row.Close()

	var name string
	row.Next()
	row.Scan(&name)

	return Item{id, name, completed}, err
}

func GetItems(db *sql.DB) ([]Item, error) {
	rows, err := db.Query("SELECT id, name, completed FROM todo_items")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	items := make([]Item, 0)
	for rows.Next() {
		var id string
		var name string
		var completed bool
		err := rows.Scan(&id, &name, &completed)
		if err != nil {
			return nil, err
		}
		items = append(items, Item{id, name, completed})
	}

	return items, nil
}

func Signup(db *sql.DB, username string, password string) (string, error) {
	id := uuid.New().String()
	hashed_password, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	log.Println("Signing up user")
	// TODO: Make sure the username is unique
	_, err = db.Exec("INSERT INTO users (id, name, hashed_password) VALUES (?, ?, ?)", id, username, hashed_password)
	return id, err
}

func Login(db *sql.DB, username string, password string) (string, error) {
	log.Println("Logging in user")
	row, err := db.Query("SELECT id, hashed_password FROM users WHERE name = ?", username)
	if err != nil {
		return "", err
	}
	defer row.Close()

	var id string
	var hashed_password string
	row.Next()
	err = row.Scan(&id, &hashed_password)
	if err != nil {
		return "", err
	}
	err = bcrypt.CompareHashAndPassword([]byte(hashed_password), []byte(password))
	if err != nil {
		return "", err
	}

	// TODO: Use a better secret
	tokenAuth := jwtauth.New("HS256", []byte("secret"), nil)
	_, tokenString, err := tokenAuth.Encode(map[string]interface{}{"user_id": id})
	return tokenString, err
}

func LoginVerifier() func(http.Handler) http.Handler {
	// TODO: Synchronize the secret and the algorithm with
	// the one used for the Login function
	tokenAuth := jwtauth.New("HS256", []byte("secret"), nil)
	return jwtauth.Verifier(tokenAuth)
}
